package com.example.icecreamshop;

public class IceCreamFlavorHandler implements IceCreamHandler{
    private IceCreamHandler next;

    public void setNext(IceCreamHandler next) {
        this.next = next;
    }

    @Override
    public void handleRequest(IceCream iceCream) {
        if (iceCream.getFlavor() == null) {
            System.out.println("Flavor not selected. Please select a flavor.");
        } else if (next != null) {
            next.handleRequest(iceCream);
        }
    }
}
