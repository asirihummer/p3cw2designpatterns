package com.example.icecreamshop;

public class ChocolateSyrup implements Syrup{
    @Override
    public void addSyrup() {
        System.out.println("Chocolate syrup added.");
    }
}
