package com.example.icecreamshop;

public class GiftWrappingDecorator extends IceCreamOrderDecorator{


    public GiftWrappingDecorator(IceCream iceCream, Customer customer, IceCreamOrder decoratedOrder) {
        super(iceCream, customer, decoratedOrder);
    }

    @Override
    public void additionalFeature() {
        System.out.println("Gift wrapper added.");
    }

    @Override
    public double getCost() {
        return decoratedOrder.getCost() + 100;
    }
}
