package com.example.icecreamshop;

public class CreditCardPayment implements PaymentStrategy{
    @Override
    public void makepayment(double amount) {
        System.out.println("Successfully paid " + amount + "/= using credit card.");
    }
}
