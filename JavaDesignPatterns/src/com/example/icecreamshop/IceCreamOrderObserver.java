package com.example.icecreamshop;

public interface IceCreamOrderObserver {

    void update(IceCreamOrder iceCreamOrder);
}
