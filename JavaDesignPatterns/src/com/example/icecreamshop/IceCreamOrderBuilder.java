package com.example.icecreamshop;

import java.util.List;

public class IceCreamOrderBuilder {
    private IceCream iceCream;

    private String deliveryAddress;

    private boolean isDelivery;

    private PaymentStrategy paymentStrategy;

    private IceCreamHandler iceCreamHandler;

    private IceCreamOrderCommand iceCreamOrderCommand;

    public IceCreamOrderBuilder selectIceCreamOrderCommand(IceCreamOrderCommand iceCreamOrderCommand) {
        this.iceCreamOrderCommand = iceCreamOrderCommand;
        return this;
    }

    public IceCreamOrderBuilder selectIceCreamHandler(IceCreamHandler iceCreamHandler) {
        this.iceCreamHandler = iceCreamHandler;
        return this;
    }

    public IceCreamOrderBuilder selectCutomizedIceCream(String flavor, List<Topping> toppings, List<Syrup> syrups){
        this.iceCream = new IceCream(flavor,toppings,syrups);
        return this;
    }

    public IceCreamOrderBuilder setDeliveryAddress(String address) {
        this.deliveryAddress = address;
        return this;
    }

    public IceCreamOrderBuilder setDelivery(boolean isDelivery) {
        this.isDelivery = isDelivery;
        return this;
    }

    public IceCreamOrderBuilder selectPaymentStrategy(PaymentStrategy paymentStrategy) {
        this.paymentStrategy = paymentStrategy;
        return this;
    }

    public IceCreamOrder build(Customer customer) {
        IceCreamOrder iceCreamOrder = new IceCreamOrder(iceCream, customer);
        iceCreamOrder.setIceCream(iceCream);
        iceCreamOrder.setPaymentStrategy(paymentStrategy);
        iceCreamOrder.setDeliveryAddress(deliveryAddress);
        iceCreamOrder.setIsDelivery(isDelivery);
        iceCreamOrder.addCommand(iceCreamOrderCommand);
        return iceCreamOrder;
    }
}
