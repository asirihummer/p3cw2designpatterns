package com.example.icecreamshop;

public class ToppingFactory {
    public Topping getTopping(String toppingType){
        if(toppingType == null){
            return null;
        }
        if(toppingType.equalsIgnoreCase("CHOCOLATE_SPRINKLE")){
            return new ChocolateSprinkle();

        } else if(toppingType.equalsIgnoreCase("RAINBOW_SPRINKLE")){
            return new RainbowSprinkle();

        }
        return null;
    }
}
