package com.example.icecreamshop;

public class LoyaltyCommand implements IceCreamOrderCommand{

    private static final int POINTS_PER_ORDER = 10;
    private Customer customer;

    public LoyaltyCommand(Customer customer) {
        this.customer = customer;
    }

    @Override
    public void executeCommand() {
        customer.addLoyaltyPoints(POINTS_PER_ORDER);
        System.out.println("Earned " + POINTS_PER_ORDER + " loyalty points for customer: " + customer.getName());
    }
}
