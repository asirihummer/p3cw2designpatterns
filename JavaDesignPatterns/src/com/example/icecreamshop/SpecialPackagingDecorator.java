package com.example.icecreamshop;

public class SpecialPackagingDecorator extends IceCreamOrderDecorator{


    public SpecialPackagingDecorator(IceCream iceCream, Customer customer, IceCreamOrder decoratedOrder) {
        super(iceCream, customer, decoratedOrder);
    }

    @Override
    public void additionalFeature() {
        System.out.println("Special wrapper added.");
    }

    @Override
    public double getCost() {
        return decoratedOrder.getCost() + 200;
    }
}
