package com.example.icecreamshop;

public interface IceCreamOrderState {
    void processOrder(IceCreamOrder iceCreamOrder);
}
