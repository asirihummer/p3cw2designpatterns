package com.example.icecreamshop;

public class StrawberrySyrup implements Syrup{

    @Override
    public void addSyrup() {
        System.out.println("Strawberry syrup added.");
    }
}
