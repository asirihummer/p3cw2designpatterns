package com.example.icecreamshop;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        System.out.println("Hello welcome to online ice cream shop!");

        Customer customer = new Customer("Sampa Withanachchi");
        CustomerNotificationObserver customerNotificationObserver = new CustomerNotificationObserver();

        IceCreamOrderBuilder orderBuilder = new IceCreamOrderBuilder();
        orderBuilder.selectCutomizedIceCream("CHOCOLATE", List.of(new ChocolateSprinkle()), List.of(new CaramelSyrup()));
        orderBuilder.setDeliveryAddress("245, 3 De Fonseka Pl, Colombo 00400")
                .setDelivery(true)
                .selectPaymentStrategy(new IcePointsPayments());

        IceCreamOrderCommand loyaltyCommand = new LoyaltyCommand(customer);
        orderBuilder.selectIceCreamOrderCommand(loyaltyCommand);

        IceCreamOrder iceCreamOrder = orderBuilder.build(customer);
        iceCreamOrder.setOrderStatus("PLACED");
        iceCreamOrder.attachObserver(customerNotificationObserver);
        iceCreamOrder.notifyObservers();

        double orderAmount = iceCreamOrder.getCost();
        iceCreamOrder.processPayment(orderAmount);
        iceCreamOrder.processCommands();
    }
}