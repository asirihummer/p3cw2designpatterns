package com.example.icecreamshop;

public class CustomerNotificationObserver implements IceCreamOrderObserver{

    @Override
    public void update(IceCreamOrder iceCreamOrder) {
        System.out.println("The order is " + iceCreamOrder.getOrderStatus());
    }
}
