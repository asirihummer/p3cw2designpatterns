package com.example.icecreamshop;

public class IceCreamSyrupHandler implements IceCreamHandler{
    private IceCreamHandler next;

    public void setNext(IceCreamHandler next) {
        this.next = next;
    }

    @Override
    public void handleRequest(IceCream iceCream) {
        if (next != null) {
            next.handleRequest(iceCream);
        }
    }
}
