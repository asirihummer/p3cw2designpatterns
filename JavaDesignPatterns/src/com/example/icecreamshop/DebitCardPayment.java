package com.example.icecreamshop;

public class DebitCardPayment implements PaymentStrategy{

    @Override
    public void makepayment(double amount) {
        System.out.println("Successfully paid " + amount + "/= using debit card.");
    }
}
