package com.example.icecreamshop;

public class SyrupFactory {

    public Syrup getSyrup(String syrupType){
        if(syrupType == null){
            return null;
        }
        if(syrupType.equalsIgnoreCase("CARAMEL_SYRUP")){
            return new CaramelSyrup();
        } else if (syrupType.equalsIgnoreCase("CHOCOLATE_SYRUP")) {
            return new ChocolateSyrup();
        } else if (syrupType.equalsIgnoreCase("STRAWBERRY_SYRUP")) {
            return new StrawberrySyrup();
        }
        return null;
    }
}
