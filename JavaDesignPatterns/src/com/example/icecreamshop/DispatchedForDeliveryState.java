package com.example.icecreamshop;

public class DispatchedForDeliveryState implements IceCreamOrderState{
    @Override
    public void processOrder(IceCreamOrder iceCreamOrder) {
        System.out.println("Ice cream order is dispatched for delivery.");
        iceCreamOrder.setOrderStatus("Dispatched for delivery");
    }
}
