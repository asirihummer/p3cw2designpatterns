package com.example.icecreamshop;

public class IcePointsPayments implements PaymentStrategy{
    @Override
    public void makepayment(double amount) {
        System.out.println("Successfully paid " + amount + "/= using IcePoints.");
    }
}
