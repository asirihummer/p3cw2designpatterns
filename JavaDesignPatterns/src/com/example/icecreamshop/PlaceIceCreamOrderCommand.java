package com.example.icecreamshop;

public class PlaceIceCreamOrderCommand implements IceCreamOrderCommand{

    private IceCreamOrder iceCreamOrder;

    public PlaceIceCreamOrderCommand(IceCreamOrder iceCreamOrder) {
        this.iceCreamOrder = iceCreamOrder;
    }

    @Override
    public void executeCommand() {
        System.out.println("Please wait, we are placing your order!");
        iceCreamOrder.setOrderStatus("Placed");
    }
}
