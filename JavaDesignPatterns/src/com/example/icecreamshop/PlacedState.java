package com.example.icecreamshop;

public class PlacedState implements IceCreamOrderState{

    @Override
    public void processOrder(IceCreamOrder iceCreamOrder) {
        System.out.println("Order is placed.");
        iceCreamOrder.setOrderStatus("Placed");
    }
}
