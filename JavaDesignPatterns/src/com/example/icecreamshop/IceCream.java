package com.example.icecreamshop;

import java.util.List;

public class IceCream {

    private String flavor;

    private List<Topping> toppings;

    private List<Syrup> syrups;
    
    public IceCream(String flavor, List<Topping> toppings, List<Syrup> syrups) {
        this.flavor = flavor;
        this.toppings = toppings;
        this.syrups = syrups;
    }

    public String getFlavor() {
        return flavor;
    }

    public List<Topping> getToppings() {
        return toppings;
    }

    public List<Syrup> getSyrups() {
        return syrups;
    }
}
