package com.example.icecreamshop;

public class GiveFeedbackCommand implements IceCreamOrderCommand{
    private String feedback;

    public GiveFeedbackCommand(String feedback) {
        this.feedback = feedback;
    }
    @Override
    public void executeCommand() {
        System.out.println("We captured your feedback: " + feedback);
    }
}
