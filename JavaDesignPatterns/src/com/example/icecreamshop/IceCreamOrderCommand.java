package com.example.icecreamshop;

public interface IceCreamOrderCommand {
    void executeCommand();
}
