package com.example.icecreamshop;

public class DeliveredState implements IceCreamOrderState{

    @Override
    public void processOrder(IceCreamOrder iceCreamOrder) {
        System.out.println("Ice cream order is delivered.");
        iceCreamOrder.setOrderStatus("Dispatched for delivery");
    }
}
