package com.example.icecreamshop;

public abstract class IceCreamOrderDecorator extends IceCreamOrder{
    public IceCreamOrderDecorator(IceCream iceCream, Customer customer, IceCreamOrder decoratedOrder) {
        super(iceCream, customer);
        this.decoratedOrder = decoratedOrder;
    }

    protected IceCreamOrder decoratedOrder;


    public abstract void additionalFeature();
}
