package com.example.icecreamshop;

public class RainbowSprinkle implements Topping{
    @Override
    public void addTopping() {
        System.out.println("Rainbow sprinkles added.");
    }
}
