package com.example.icecreamshop;

public class SaveFavoriteIceCreamCombinationCommand implements IceCreamOrderCommand{
    private IceCreamOrder iceCreamOrder;
    private String favoriteCombinationName;

    public SaveFavoriteIceCreamCombinationCommand(IceCreamOrder iceCreamOrder, String favoriteName) {
        this.iceCreamOrder = iceCreamOrder;
        this.favoriteCombinationName = favoriteName;
    }

    @Override
    public void executeCommand() {
        System.out.println("Your favorite ice cream combination: " + favoriteCombinationName);
    }
}
