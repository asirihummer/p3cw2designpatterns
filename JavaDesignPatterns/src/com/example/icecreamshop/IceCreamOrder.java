package com.example.icecreamshop;

import java.util.ArrayList;
import java.util.List;

public class IceCreamOrder {

    private IceCream iceCream;

    private List<Topping> toppings;

    private List<Syrup> syrups;

    private boolean isDelivery;

    private List<IceCreamOrderObserver> orderobservers = new ArrayList<>();

    private List<IceCreamOrderCommand> commandList = new ArrayList<>();

    private String orderstatus;

    private PaymentStrategy paymentStrategy;

    private IceCreamHandler iceCreamHandler;

    private Customer customer;

    public IceCreamOrder(IceCream iceCream, Customer customer) {
        this.iceCream = iceCream;
        this.customer = customer;
    }

    public void setIceCreamHandler(IceCreamHandler iceCreamHandler) {
        this.iceCreamHandler = iceCreamHandler;
    }

    public void processRequest() {
        if (iceCreamHandler != null) {
            iceCreamHandler.handleRequest(iceCream);
        } else {
            System.out.println("Unable to process the request.");
        }
    }

    public void processPayment(double amount) {
        if (paymentStrategy != null) {
            paymentStrategy.makepayment(amount);
        } else {
            System.out.println("Payment method not set. Unable to process payment.");
        }
    }

    public void setPaymentStrategy(PaymentStrategy paymentStrategy) {
        this.paymentStrategy = paymentStrategy;
    }

    public void attachObserver(IceCreamOrderObserver orderobserver) {
        orderobservers.add(orderobserver);
    }

    public void notifyObservers() {
        for (IceCreamOrderObserver observer : orderobservers) {
            observer.update(this);
        }
    }

    public void addCommand(IceCreamOrderCommand command) {
        commandList.add(command);
    }

    public void processCommands() {
        for (IceCreamOrderCommand command : commandList) {
            command.executeCommand();
        }
    }

    public void setOrderStatus(String status) {
        this.orderstatus = status;
        notifyObservers();
    }

    public String getOrderStatus() {
        return orderstatus;
    }

    public double getCost() {
        return calculateBaseCost();
    }

    private double calculateBaseCost() {
        double iceCreamCost = calculateIceCreamCost();
        double toppingsCost = calculateToppingsCost();
        double syrupsCost = calculateSyrupsCost();

        return iceCreamCost + toppingsCost + syrupsCost;
    }

    private double calculateIceCreamCost() {
        String flavor = iceCream.getFlavor();

        double cost;
        switch (flavor) {
            case "CHOCOLATE":
                cost = 200.00;
                break;
            case "VANILLA":
                cost = 100.00;
                break;
            case "STRAWBERRY":
                cost = 150.00;
                break;
            default:
                cost = 0.00;
                break;
        }

        return cost;
    }

    private double calculateToppingsCost() {
        List<Topping> selectedToppings = iceCream.getToppings();
        double totalToppingsCost = 0.00;
        for (Topping topping : selectedToppings) {
            totalToppingsCost += calculateToppingCost(topping);
        }
        return totalToppingsCost;
    }

    private double calculateToppingCost(Topping topping) {
        double cost = 0.00;

        ToppingFactory toppingFactory = new ToppingFactory();

        if (topping instanceof ChocolateSprinkle) {
            cost = 30.00;
        } else if (topping instanceof RainbowSprinkle) {
            cost = 20.00;
        }

        return cost;
    }

    private double calculateSyrupsCost() {
        List<Syrup> selectedSyrups = iceCream.getSyrups();
        double totalSyrupsCost = 0.00;
        for(Syrup syrup : selectedSyrups){
            totalSyrupsCost += calculateSyrupCost(syrup);
        }
        return totalSyrupsCost;
    }

    private double calculateSyrupCost(Syrup syrup) {
        double cost = 0.00;

        SyrupFactory syrupFactory = new SyrupFactory();

        if (syrup instanceof ChocolateSyrup) {
            cost = 40.00;
        } else if (syrup instanceof CaramelSyrup) {
            cost = 45.00;
        } else if (syrup instanceof StrawberrySyrup) {
            cost = 35.00;
        }

        return cost;
    }

    public IceCream getIceCream() {
        return iceCream;
    }

    public void setIceCream(IceCream iceCream) {
        this.iceCream = iceCream;
    }

    public List<Topping> getToppings() {
        return toppings;
    }

    public void setToppings(List<Topping> toppings) {
        this.toppings = toppings;
    }

    public List<Syrup> getSyrups() {
        return syrups;
    }

    public void setSyrups(List<Syrup> syrups) {
        this.syrups = syrups;
    }

    public String getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    private String deliveryAddress;

    public boolean isDelivery() {
        return isDelivery;
    }

    public void setIsDelivery(boolean delivery) {
        isDelivery = delivery;
    }
}
