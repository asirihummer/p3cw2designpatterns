package com.example.icecreamshop;

public class ChocolateSprinkle implements Topping{
    @Override
    public void addTopping() {
        System.out.println("Chocolate sprinkles added.");
    }
}
