package com.example.icecreamshop;

public class PreparingState implements IceCreamOrderState {
    @Override
    public void processOrder(IceCreamOrder iceCreamOrder) {
        System.out.println("Ice cream order is being prepared.");
        iceCreamOrder.setOrderStatus("Preparing");
    }
}
