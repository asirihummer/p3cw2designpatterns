package com.example.icecreamshop;

public interface IceCreamHandler {
    void handleRequest(IceCream iceCream);
}
